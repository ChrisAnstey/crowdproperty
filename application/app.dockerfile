#
# PHP Dependency install stage
#
FROM composer:1.9 as phpdep

COPY database/ database/

COPY composer.json composer.json
COPY composer.lock composer.lock

# Install PHP dependencies in 'vendor'
RUN composer install \
    --ignore-platform-reqs \
    --no-dev \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --prefer-dist

#
# Final image build stage
#
FROM php:7.2-fpm

COPY . /var/www
COPY --from=phpdep /app/vendor/ /var/www/vendor/

RUN chown -R www-data:www-data \
        /var/www/storage \
        /var/www/bootstrap/cache

RUN docker-php-ext-install pdo_mysql

