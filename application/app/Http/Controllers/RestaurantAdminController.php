<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Restaurant;
use App\Http\Requests\SubmitRestaurant;

class RestaurantAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurants = Restaurant::all();
        return view('restaurantadmin/index', ["restaurants" => $restaurants]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('restaurantadmin/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SubmitRestaurant  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubmitRestaurant $request)
    {
        $restaurant = new Restaurant;
        $restaurant->name = $request->input('name');
        $restaurant->location = $request->input('location');
        $restaurant->save();

        return redirect()->route('backend.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function show(Restaurant $restaurant)
    {
         return view('restaurantadmin/show', ["restaurant" => $restaurant]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function edit(Restaurant $restaurant)
    {
        return view('restaurantadmin/edit', ["restaurant" => $restaurant]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SubmitRestaurant  $request
     * @param  \App\Models\Restaurant    $restaurant
     * @return \Illuminate\Http\Response
     */
    public function update(SubmitRestaurant $request, Restaurant $restaurant)
    {
        $restaurant->name = $request->input('name');
        $restaurant->location = $request->input('location');
        $restaurant->save();

        return redirect()->route('backend.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Restaurant $restaurant)
    {
        $restaurant->delete();

        return redirect()->route('backend.index');
    }
}
