<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Restaurant;

class FrontendController extends Controller
{
    /**
     * Allow user to search for closest restaurant.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurants = Restaurant::all();
        return view('frontend/index', ["restaurants" => $restaurants]);
    }
}
