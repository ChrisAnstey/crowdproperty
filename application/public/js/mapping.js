var map;
var geocoder;
var directionsService;
var directionsRenderer;

// Initialize and add the map
function initMap() {
  // The default location
  var default_location = {lat: 52.4862, lng: -1.8904};
  map = new google.maps.Map(
      document.getElementById('map'), {zoom: 12, center: default_location});
  geocoder = new google.maps.Geocoder();
  directionsService = new google.maps.DirectionsService();
  directionsRenderer = new google.maps.DirectionsRenderer();
  directionsRenderer.setMap(map);

}

// Handle requests to find closest restaurant
function renderDirections(restaurants_locations) {

    if (restaurants_locations.length == 0) {
        alert('Sorry, there are no restaurants available.');
        return;
    }
    var desired_location = document.getElementById('location').value;
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
      {
        origins: [desired_location],
        destinations: restaurants_locations,
        travelMode: 'DRIVING',
      },
      renderDirectionsCallback);
}

// Decide closest restaurant and render directions on map
function renderDirectionsCallback(response, status) {
    if (status != 'OK') {
      alert ("Directions request failed: " + status);
    }
    var destinations = response.destinationAddresses;
    var results = response.rows[0].elements;
    var min_distance = null;

    // loop through results, to find closest restaurant
    for (var j = 0; j < results.length; j++) {
      if (results[j].status != 'OK') {
        alert ("Directions request failed: " + results[j].status);
      }
      var distance = results[j].distance.value;
      if (!min_distance || distance < min_distance) {
          min_distance = distance;
          closest_destination = destinations[j];
      }
    }

    // create a request object to get directions details
    var request = {
      origin: document.getElementById('location').value,
      destination: closest_destination,
      travelMode: 'DRIVING'
    };

    // get directions, and render details
    directionsService.route(request, function(result, status) {
      if (status == 'OK') {
        directionsRenderer.setDirections(result);
      } else {
        alert ("Directions request failed:" + status);
      }
    });
}

// get clients location if possible, then update input box, and trigger render of directions
function getClientsLocation() {
    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function(position) {
          document.getElementById('location').value = "" + position.coords.latitude + " " + position.coords.longitude;
        });
    } else {
      alert('Unable to get location');
    }
}
