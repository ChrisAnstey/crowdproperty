@extends('layouts.app')

@section('title', 'Restaurant Admin - Index')

@section('content')
    <h4>Restaurant Admin - Index</h4>
    <div class="pb-3">
        <a class="btn btn-primary" href="{{ route('backend.create') }}">Add Restaurant</a>
    </div>

    @forelse ($restaurants as $restaurant)
        @if ($loop->first)
            <table class="table table-striped">
            <tr>
                <th>Name</th>
                <th>Location</th>
                <th colspan="3" style="width: 10%">Actions</th>
            </tr>
        @endif
        <tr>
            <td>{{ $restaurant->name }}</td>
            <td>{{ $restaurant->location }}</td>
            <td><a class="btn btn-primary" href="{{ route('backend.show', $restaurant->id) }}">View</a></td>
            <td><a class="btn btn-primary" href="{{ route('backend.edit', $restaurant->id) }}">Edit</a></td>
            <td>
                <form method="post" action="{{ route('backend.destroy', $restaurant->id) }}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-primary">Delete</button>
                </form>
            </td>

        </tr>
        @if ($loop->last)
            </table>
        @endif
    @empty
        No Restaurants
    @endforelse
@endsection
