@extends('layouts.app')

@section('title', 'Restaurant Admin - Index')

@section('content')
    <h4>Restaurant Admin - View</h4>
    <b>Name:</b> {{ $restaurant->name }}
    <br />
    <b>Location:</b> {{ $restaurant->location }}
    <br />
    <a class="btn btn-primary mb-3" href="{{ route('backend.edit', $restaurant->id) }}">Edit</a>
    <br />
    <form method="post" action="{{ route('backend.destroy', $restaurant->id) }}">
        @csrf
        @method('DELETE')
        <button class="btn btn-primary" type="submit">Delete</button>
    </form>
@endsection
