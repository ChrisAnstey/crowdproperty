@extends('layouts.app')

@section('title', 'Restaurant Admin - Edit')

@section('content')
    <h4>Restaurant Admin - Edit</h4>
    <form action="{{ route('backend.update', $restaurant->id) }}" method="POST">
        @method('PUT')
        @csrf

        <label for="name">Name: </label>
        <input id="name" type="text" name="name" value="{{ old('name', $restaurant->name) }}" class="form-control" placeholder="Restaurant name" />
        @if ($errors->has('name'))
            <div class="text-danger">{{ $errors->first('name') }}</div>
        @endif
        <br />
        <label for="location">Location: </label>
        <input type="text" id="location" name="location" value="{{ old('restaurant', $restaurant->location) }}" class="form-control" placeholder="Restaurant location" />
        @if ($errors->has('location'))
            <div class="text-danger">{{ $errors->first('location') }}</div>
        @endif
        <br />
        <input class="btn btn-primary" type="submit" value="Submit">
    </form>
@endsection
