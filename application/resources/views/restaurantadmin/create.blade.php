@extends('layouts.app')

@section('title', 'Restaurant Admin - Create')

@section('content')
    <h4>Restaurant Admin - Create</h4>
    <form action="{{ route('backend.store') }}" method="POST">
        @csrf

        <label for="name">Name: </label>
        <input id="name" type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Restaurant name" required />
        @if ($errors->has('name'))
            <div class="text-danger">{{ $errors->first('name') }}</div>
        @endif
        <br />
        <label for="location">Location: </label>
        <input type="text" id="location" name="location" value="{{ old('location') }}" class="form-control" placeholder="Restaurant location" required />
        @if ($errors->has('location'))
            <div class="text-danger">{{ $errors->first('location') }}</div>
        @endif
        <br />
        <input class="btn btn-primary" type="submit" value="Submit">
    </form>
@endsection
