@extends('layouts.app')

@section('title', 'Restaurant Finder')

@section('content')
    <div class="col-xs-1 text-center mt-3">
        <h3>
            Restaurant Search
        </h3>
        <button onclick="getClientsLocation()">Get Current Location</button>
        <input id="location" type="text" />
        <button type="submit" onclick="renderDirections(restaurants_locations)">Go</button>
    </div>
    <!--The div element for the map -->
    <div id="map"></div>
    <script>
        var restaurants_locations = {!! json_encode($restaurants->pluck('location')) !!};
    </script>
    <script src="/js/mapping.js"></script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key={{ config('app.google_map_api_key') }}&callback=initMap">
    </script>

@endsection
