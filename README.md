# Crowd Restaurants

A simple tool to allow visitors to input their location, or use a handy "Use current location" button, then find directions to the nearest restaurant in the system. There is an admin backend at `/backend`.

## Instructions

Customise `docker-compose.yml` as necessary, then run:
```
docker-compose up --build
```

Note - You'll need to have an SSL cert and use `https` to get the geolocation to work.

A Google Maps API key will need to be added via dotenv. It will need access to the following APIs:

* Distance Matrix API

* Maps JavaScript API

* Geocoding API

* Directions API